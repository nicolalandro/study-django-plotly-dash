from django.shortcuts import render
from django_plotly_dash import DjangoDash
import plotly.graph_objects as go

from dash import Dash, dcc, html, Input, Output
import plotly.express as px
from urllib.parse import urlparse, parse_qs

app = DjangoDash('Simple3DExample')
app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'href')])
def display_page(href):
    fig = go.Figure()

    # PD
    fig.add_trace(
        go.Mesh3d(
            # 8 vertices of a cube
            x=[0,  0,     0.1,   0.1,     0, 0, 0.1, 0.1],
            y=[0,  0,     0, 0,   0.2,     0.2,     0.2,   0.2, ],
            z=[0,  0.01, 0.01,   0, 0,     0.01, 0.01,     0, ],

            i=[7, 0, 0, 0, 4, 4, 6, 6, 4, 0, 3, 2],
            j=[3, 4, 1, 2, 5, 6, 5, 2, 0, 1, 6, 3],
            k=[0, 7, 2, 3, 6, 7, 1, 1, 5, 5, 7, 6],
            opacity=0.3,
            color='#DC143C',
            flatshading=True
        ),
    )

    # T3
    fig.add_trace(
        go.Mesh3d(
            # 8 vertices of a cube
            x=[4,   4,     10,     10,   4,     4,     10,   10, ],
            y=[1,   1,     1,    1,  10,   10,     10,    10, ],
            z=[0.1, 0.2,  0.2,   0.1, 0.1, 0.2,  0.2,   0.1],

            i=[7, 0, 0, 0, 4, 4, 6, 6, 4, 0, 3, 2],
            j=[3, 4, 1, 2, 5, 6, 5, 2, 0, 1, 6, 3],
            k=[0, 7, 2, 3, 6, 7, 1, 1, 5, 5, 7, 6],
            opacity=0.3,
            color='black',
            flatshading=True
        )
    )

    fig.update_layout(
        scene=dict(
            xaxis_title='CH4/H2',
            xaxis_type="log",
            yaxis_title='C4H2/C4H6',
            yaxis_type="log",
            zaxis_title='C2H2/C2H4'
        ),
        width=700,
        margin=dict(r=20, b=20, l=20, t=20),
        scene_aspectmode='manual',
        scene_aspectratio=dict(x=1, y=1, z=1),
    )

    parsed_url = urlparse(href)
    inp = parse_qs(parsed_url.query)
    print('!!!!!', inp)
    if all(elem in inp.keys() for elem in ['x', 'y', 'z']):
        inp = {k: [float(i) for i in v] for k,v in inp.items()}
        print('!!!!!', inp)
        fig.add_trace(
            go.Scatter3d(x=inp['x'], y=inp['y'], z=inp['z'])
        )
    return html.Div([
        dcc.Graph(figure=fig,  config=dict(
            displayModeBar=False
        ))
    ], style={
        "width": 400,
        "height": 800
    })


def index3d(request):
    return render(request, 'plotly_example/index3d.html')
