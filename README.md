# Django dash

```
python3.10 -m venv venv
source venv/bin/activate.fish
pip install -r requirements.txt
cd app
./manage.py runserver
# http://localhost:8000/plotly_example/1
# http://localhost:8000/plotly_example/2
# localhost:8000/django_plotly_dash/app/Simple3DExample/?x=0.1&y=0.1&z=0.1
# localhost:8000/django_plotly_dash/app/Simple3DExample/
```

# References

* [django plotly dash](https://django-plotly-dash.readthedocs.io/en/latest/)
* [Plotly get url](https://dash.plotly.com/dash-core-components/location)